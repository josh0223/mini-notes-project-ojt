

import UIKit
import CoreData


class NotesManager {
    
    static let sharedInstance = NotesManager()
    init(){}
    let ENTITY = "Notes"
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    lazy var context = appDelegate.persistentContainer.viewContext
    lazy var entity = NSEntityDescription.entity(forEntityName: ENTITY, in: context)
    

    func saveNotes(_ notesDetails: NotesModel, notesDictionary: [String: Any]){
            let notesManageObject = NSManagedObject(entity: entity!, insertInto: context) as! Notes
        
//        let imageData = image.jpegData(compressionQuality: 0.1)! as NSData
        ///convert image to data
        let image = UIImage()
        let imageData = image.jpegData(compressionQuality: 0.1)
       ///convert data to image
        _ = UIImage(data: imageData!)

            notesManageObject.title_name = notesDetails.notesTitleName
            notesManageObject.body = notesDetails.notesBody
            notesManageObject.note_image = notesDetails.imageData

            do{
                try context.save()
            }
            catch {
                print("error")
            }
        }
    
    func getAllNotes() -> [NotesModel]{
           var notesArray = [NotesModel]()
           let request = NSFetchRequest<NSFetchRequestResult>(entityName: ENTITY)
           do {
               let fetchNotes = try context.fetch(request)
               if fetchNotes.count > 0{
                   notesArray = fetchNotes.map({NotesModel(managedObjects: $0 as! NSManagedObject)})
               }
           }
           catch{
               print("error")
           }
           
           return notesArray
       }

    func deleteNotes(_ deletenotes: NotesModel){
    let requesst = NSFetchRequest<NSFetchRequestResult>(entityName: ENTITY)
        requesst.predicate = NSPredicate(format: "title_name == %@", deletenotes.notesTitleName)
           do {
            let notesItem = try context.fetch(requesst)
            if notesItem.count > 0 {
                notesDelete(notesItem[0] as! Notes)
            }
           }
            catch
            {
                print("Error")
            }
        }
    
      func notesDelete(_ noteDelete: Notes){
            context.delete(noteDelete)
            do{
                try context.save()
            }
            catch{
            }
        }
    
    
    
//class NotesManager{
//    var image: UIImage?=nil
//
//    func saveImage(){
//        let appDelegate = (UIApplication.shared.delegate) as! AppDelegate
//        let context = appDelegate.persistentContainer.viewContext
//        let imageObject = NSEntityDescription.insertNewObject(forEntityName: "Notes", into: context) as! Notes
//        imageObject.note_image = image?.jpegData(compressionQuality: 1) as NSData? as Data?
//
//        do{
//            try context.save()
//            print("Succesfully Inserted")
//        } catch {
//            print("Failed to save Image")
//        }
//    }
//
//    func retrieveImage() -> [Notes]{
//        var imagenote = [Notes]()
//        let appDelegate = (UIApplication.shared.delegate) as! AppDelegate
//        let context = appDelegate.persistentContainer.viewContext
//
//        do{
//            imagenote = try (context.fetch(Notes.fetchRequest()))
//        } catch {
//            print("Error while fetching Data")
//        }
//        return imagenote
//    }
//
} //end of class
