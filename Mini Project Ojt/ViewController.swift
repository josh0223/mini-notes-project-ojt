//
//  ViewController.swift
//  Mini Project Ojt
//
//  Created by Hive Developer on 1/4/22.
//  Copyright © 2022 Hive Developer. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func didTapNextPage(_ sender: Any) {
        let logInViewControl = LoginViewController()
        self.navigationController?.pushViewController(logInViewControl, animated: true)
    }
    
    @IBAction func didTapRegister(_ sender: Any) {
        let RegisterVc = RegisterViewController()
        self.navigationController?.pushViewController(RegisterVc, animated: true)
    }
    
    @IBAction func didTapNotesView(_ sender: Any) {
        let viewNotesVc = NotesTableViewController()
        self.navigationController?.pushViewController(viewNotesVc, animated: true)
    }
    
    
    @IBAction func didTapAddNotes(_ sender: Any) {
        let addNotesVc = AddNotesViewController()
              self.navigationController?.pushViewController(addNotesVc, animated: true)
    }
    
    
    
}
