import UIKit
import CoreData

struct NotesModel{
    var notesTitleName: String!
    var notesBody: String!
    var imageData: Data!
    var user_id: String!
//    var location: LocationModel!
    
    init (json: [String: Any]){
        self.notesTitleName = json["title_name"] as? String ?? ""
        self.notesBody = json["body"] as? String ?? ""
        self.imageData = json["note_image"] as? Data
//        self.imageData = json["note_image"] as? UIImage
    }
    
    init(managedObjects: NSManagedObject){
        self.notesTitleName = managedObjects.value(forKey: "title_name") as? String ?? ""
        self.notesBody = managedObjects.value(forKey: "body") as? String ?? ""
        self.imageData = managedObjects.value(forKey: "note_image") as? Data
    }
}



