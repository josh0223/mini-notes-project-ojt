
import UIKit

class NotesTableViewController: UIViewController {

    //table
    @IBOutlet weak var mainTableView: UITableView!
    //cell
    let CELL_IDENTIFIER = "NotesTableViewCell"
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var delegate: NotesTableViewController!
    
    var notesModel: [NotesModel]?
    var notesManager = NotesManager()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setUpView()
        fetchNotes()
    }
    
    func setUpView(){
        mainTableView.register(UINib(nibName: CELL_IDENTIFIER, bundle: nil), forCellReuseIdentifier: CELL_IDENTIFIER)
        mainTableView.delegate = self
        mainTableView.dataSource = self
    }
    
    func deleteNote(index: Int){ notesManager.deleteNotes(notesModel![index])
        fetchNotes()
    }
    
    func fetchNotes(){
          notesModel = notesManager.getAllNotes()
          self.mainTableView.reloadData()
      }
    

    @IBAction func didTapAddNotes(_ sender: Any) {
        let addNotesTableView = AddNotesViewController()
        addNotesTableView.delegate = self
        self.navigationController?.pushViewController(addNotesTableView, animated:true)
    }
    
    
    
} //end of class


//extension here
extension NotesTableViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.notesModel?.count ?? 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER, for: indexPath) as! NotesTableViewCell

        let noteslist = self.notesModel![indexPath.row]
        cell.titleLabel.text = noteslist.notesTitleName
        cell.bodyLabel.text = noteslist.notesBody
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         return 50
    }
    
    //did select the data from table view, this will show
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let addNotesViewController = AddNotesViewController()
    self.present(addNotesViewController, animated: true, completion: nil)
    }
    
    
    //delete
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
        deleteNote(index: indexPath.row)
        }

    }
}

extension NotesTableViewController: AddNotesViewControllerDelegate{
    func didSuccessfullyAddNotes(_ notesDictionary: [String : Any]) {
    fetchNotes()
    }
    
    
}









