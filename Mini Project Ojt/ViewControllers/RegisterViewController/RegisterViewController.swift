
import FirebaseAuth
import UIKit

class RegisterViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
func validateFields() -> String? {
       
   // Check that all fields are filled in
   if emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
      passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
      confirmPasswordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
    
    let alert = UIAlertController(title: "", message: ("Fill in all fields."),
                                  preferredStyle: UIAlertController.Style.alert)
    
    alert.addAction(UIAlertAction(title: "Close",
                                  style: UIAlertAction.Style.default,
                                  handler: {(_: UIAlertAction!) in
    }))
    self.present(alert, animated: true, completion: nil)
    
   }
    
    if passwordTextField.text != confirmPasswordTextField.text {
        let alert = UIAlertController(title: "", message: ("Password not match."),
                                                preferredStyle: UIAlertController.Style.alert)
                  
        alert.addAction(UIAlertAction(title: "Close",
                                                style: UIAlertAction.Style.default,
                                                handler: {(_: UIAlertAction!) in
        }))
        self.present(alert, animated: true, completion: nil)
    }
       return nil
}
    
@IBAction func didTapSignUp(_ sender: Any) {
    _ = validateFields()
    
    // Create cleaned versions of the data
    let email = emailTextField.text!
    let password = passwordTextField.text!
    
    // Create the user
    Auth.auth().createUser(withEmail: email, password: password) { (result, err) in
        
    if err != nil {
        
    // There was an error creating the user
    let alert = UIAlertController(title: "", message: ("Error creating user"),
                                                preferredStyle: UIAlertController.Style.alert)
                  
      alert.addAction(UIAlertAction(title: "Close",
                                    style: UIAlertAction.Style.default,
                                    handler: {(_: UIAlertAction!) in
      }))
      self.present(alert, animated: true, completion: nil)
        
    }else{
        
        let alert = UIAlertController(title: "", message: ("Succefully register"),
                                      preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Close",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                    
                                        
        }))

        self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
}

    
