
import UIKit


protocol AddNotesViewControllerDelegate {
    func didSuccessfullyAddNotes(_ notesDictionary: [String: Any])
}

class AddNotesViewController: UIViewController {

    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var bodyTextField: UITextField!
    
    @IBOutlet weak var imageViewImage: UIImageView!
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
      
      var notesModel: [NotesModel]?
      var notesManager = NotesManager()
    
      var delegate: AddNotesViewControllerDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

    
    @IBAction func didTapAddImage(_ sender: Any) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        present(imagePicker,animated: true)
    }
    
    @IBAction func didTapSave(_ sender: Any) {
        var notesDictionary = [String: Any]()
        notesDictionary["title_name"] = titleTextField.text
        notesDictionary["body"] = bodyTextField.text
        notesDictionary["note_image"] = imageViewImage.image
        
        let notesModel = NotesModel(json: notesDictionary)
        
        notesManager.saveNotes(notesModel, notesDictionary: notesDictionary)
         
        if delegate != nil {
        delegate.didSuccessfullyAddNotes(notesDictionary)
        }
        self.navigationController?.popViewController(animated: true)
    }
    
} //end of class
///delegate for image picker

extension AddNotesViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey(rawValue:"UIImagePickerControllerEditedImage")] as? UIImage{
            imageViewImage.image = image
        }
        picker.dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}

