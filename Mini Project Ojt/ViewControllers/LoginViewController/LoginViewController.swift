
import FirebaseAuth
import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var iconImageView: UIImageView!
    
    @IBOutlet weak var signEmailTextField: UITextField!
    @IBOutlet weak var signPasswordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpView()
    }


func setUpView(){
        iconImageView.image = UIImage(named: "noted")
    }

    func validateFieldss() -> String? {
        
    // Check that all fields are filled in
    if signEmailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
       signPasswordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
     
     let alert = UIAlertController(title: "", message: ("Fill in all fields."),
                                   preferredStyle: UIAlertController.Style.alert)
     
     alert.addAction(UIAlertAction(title: "Close",
                                   style: UIAlertAction.Style.default,
                                   handler: {(_: UIAlertAction!) in
     }))
     self.present(alert, animated: true, completion: nil)
    }
    return nil
}

@IBAction func didTapLogin(_ sender: Any) {
      _ = validateFieldss()
    
    let email = signEmailTextField.text!
    let password = signPasswordTextField.text!
    
    // Signing in the user
    FirebaseAuth.Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
        
        if error != nil {
            // Couldn't sign in
        let alert = UIAlertController(title: "", message: ("Error creating user"),
                                                    preferredStyle: UIAlertController.Style.alert)
                      
          alert.addAction(UIAlertAction(title: "Close",
                                        style: UIAlertAction.Style.default,
                                        handler: {(_: UIAlertAction!) in
          }))
          self.present(alert, animated: true, completion: nil)
            
        }else{
            
            let alert = UIAlertController(title: "", message: ("Succefully Login"),
                                          preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: "Close",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                     
            }))

            self.present(alert, animated: true, completion: nil)
            }
      
        }
    }
    
    @IBAction func didTapRegisterLink(_ sender: Any) {
        let registerVC = RegisterViewController()
        self.navigationController?.pushViewController(registerVC, animated: true)
    }
} //class
